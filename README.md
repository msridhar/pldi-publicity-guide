PLDI Publicity Guide
====================

This is a guide for doing publicity for
[PLDI](http://www.sigplan.org/Conferences/PLDI/), mostly useful to the
publicity chair.

The primary tasks of the publicity chair are:
1. Email calls for papers, participation, etc. to a number of mailing
   lists.
2. Post occasionally on the PLDI social media accounts.

Further details below.

### Mailing Lists

Note: some mailing lists require that you subscribe in order to send
messages.  You may want to create a separate email address from which
you send publicity emails.  Lindsey Kuper has written some other good
tips in
[this blog post](http://composition.al/blog/2016/07/31/publicity-chair-tip-use-ruby-gmail-to-send-bulk-emails-via-gmail/).

The main mailing lists that have been used in the past are (in rough
order of importance): 

* [Types-announce](http://lists.seas.upenn.edu/mailman/listinfo/types-announce)
* [SEWORLD](http://www.sigsoft.org/resources/seworld.html)
* [EAPLS](http://eapls.org/)
* [HiPEAC-Publicity](http://lists.hipeac.net/mailman/listinfo/hipeac-publicity)
* [Artist Mailing List](http://www.artist-embedded.org/artist/Artist-Mailing-List,995.html)

SEWORLD is picky about what you send; you are allowed one combined
call for papers / workshops / tutorials, and one call for
participation for the main conference and all co-located events.

Additionally, you should send the major announcements to the current
SIGPLAN information director (see
[here](http://sigplan.org/ContactUs/)) and ask to have them included
in the monthly SIGPLAN email newsletter.

### Social Media

PLDI has a [Twitter account](https://twitter.com/PLDI) and a
[Facebook page](https://www.facebook.com/PLDIConf).  Any announcements
posted to mailing lists should also be posted to these accounts.
Since it is more targeted to people specifically in the PLDI
community, social media can also be used for extra reminders, e.g.,
when the submission deadline or early registration deadline is
approaching.  It's also nice to make announcements regarding the
number of submissions, number of accepted papers, a link to the
accepted papers list when available, and so on.

During the conference, it is nice to keep the PLDI Twitter account
active.  It can be used to announce conference events as they are
happening, and also to retweet what others are writing about the
conference.  You can scroll through
[the account's history](https://twitter.com/PLDI) to see what was done
in previous conferences.

